

from pyislamia import qibla, astroOps
from pytz import timezone
from datetime import datetime
from astronomia import calendar,util, sun, coordinates,nutation,dynamical
from pyislamia.tool import frac_of_day, jd_to_datetime,d_to_dms



def main():
    '''timezone'''
    tz = timezone("Asia/Jakarta")
    '''today datetime'''
#     dt = datetime.today()
    dt = datetime(year=2013,month=1,day=1)
    jd = calendar.cal_to_jde(dt.year, dt.month, dt.day, dt.hour, dt.minute,dt.second) #julian day in local_time
    utc_jd = jd - frac_of_day(tz.utcoffset(dt)) #julian_day UTC
  
    """date time format"""
    fmt = '%Y-%m-%d'
    hour_fmt ='%H:%M:%S %Z'
    full_fmt = fmt + " " + hour_fmt
    
    print "========calculate qibla direction and qibla hour==========="
    
    """today datetime"""
    print "Date and Time\t\t: %s" %  tz.localize(dt).strftime(full_fmt)
    
    
    '''Observer Latitude and longitude in [degree, minute, second]'''
    dms_lon =[107, 37, 0]
    dms_lat =[-6, 57, 0]
    
    ''' date format for displaying yyyy-mm-dd HH:MM:SS, 
        e.g. 2014-03-12 20:05:30 WIB'''
    dateformat="%Y-%m-%d %H:%M:%S %Z"
    
    
    '''Longitude 107 37' converted to radian'''
    obs_lon = util.d_to_r(util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2])) 
    '''Latitude -6 57' converted to radian'''
    obs_lat = util.d_to_r(util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 

#     print u"Observer's position\t: %f\xb0 %f\xb0" % (util.dms_to_d(dms_lon[0],dms_lon[1],dms_lon[2]),util.dms_to_d(dms_lat[0],dms_lat[1],dms_lat[2])) 
    
    print u"Observer's position\t: %d\xb0%d'%d\" %d\xb0%d'%d\"" % (dms_lon[0],dms_lon[1],dms_lon[2],dms_lat[0],dms_lat[1],dms_lat[2])
    
    qibla_dir = util.r_to_d(qibla.qiblaDirection(obs_lon,obs_lat))
    
    dms_qibla = d_to_dms(qibla_dir)
    
    
#     print u"Qibla from True North\t:%f\xb0" % qibla_dir
    
    print u"Qibla from True North\t: %d\xb0%d'%d\"" % (dms_qibla[0],dms_qibla[1],dms_qibla[2])
    
    jd_qibla = qibla.qiblaHour(jd, obs_lon, obs_lat, tz.utcoffset(dt))

    if jd_qibla is not None:    
        print u"Qibla hour\t\t: %s" % tz.localize(jd_to_datetime(jd_qibla)).strftime(full_fmt)
        
    
        print "========Checking sun horizontal coord at qibla hour=========="
        
        _sun = sun.Sun()
        '''qibla hour in UTC'''
        ut1 = jd_qibla - frac_of_day(tz.utcoffset(dt)) 
                                     
        '''ecliptical position'''
        ecl_pos = _sun.dimension3(ut1)
        '''obliquity'''
        obliquity = nutation.obliquity(ut1)
        '''equatorial position'''
        equ_pos = coordinates.ecl_to_equ(ecl_pos[0], ecl_pos[1], obliquity) 
        '''horizontal position'''
        AzAlt = astroOps.equ_to_horiz(astroOps.calc_hour_angle(ut1, obs_lon, equ_pos[0]),
                                      equ_pos[1], 
                                      obs_lat)
        '''convert to degree'''
        AzAlt = [util.r_to_d(x) for x in AzAlt]
        '''normalize azimuth'''
        AzAlt = [util.mod360(AzAlt[0]), AzAlt[1]]
        AzAlt = [d_to_dms(x) for x in AzAlt] 
        print u"Sun Horizontal Coord.\t: Azimuth=%d\xb0%d'%d\", Altitude=%d\xb0%d'%d\"" % (AzAlt[0][0],AzAlt[0][1],AzAlt[0][2],\
                                                                              AzAlt[1][0],AzAlt[1][1],AzAlt[1][2])
    else:
        print "Qibla hour doesn't happen."
   


if __name__== "__main__":
    main() 

   